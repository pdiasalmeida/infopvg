#include <osg/Notify>

#include <osgEarthUtil/EarthManipulator>
#include <osgEarthUtil/ExampleResources>

#include <osgEarthDrivers/tms/TMSOptions>

#include "UI/MainWindow.hpp"
#include "UI/MyTableViewPopup.hpp"

#include <QApplication>

#ifdef Q_WS_X11
#include <X11/Xlib.h>
#endif

#include "VGViewer.hpp"
#include "Models/ModelHandler.hpp"
#include "Layers/GeoTiffHandler.hpp"

#define LC "[INFOPVG] "

int
usage(const char* name)
{
    OE_NOTICE
        << "\nUsage: " << name << " file.earth" << std::endl
        << osgEarth::Util::MapNodeHelper().usage() << std::endl;

    return 0;
}

int main( int argc, char** argv )
{
	osg::ArgumentParser arguments(&argc,argv);

	// help?
	if ( arguments.read("--help") )
	{
		return usage(argv[0]);
	}

	#ifdef Q_WS_X11
	XInitThreads();
	#endif

	VGViewer pvgViewer;
	pvgViewer.setupVirtualGlobe();
	QApplication app(argc, argv);

	osgEarth::Util::MapNode* mapNode = pvgViewer.getMapNode();
	osg::ref_ptr<osg::Group> rootNode = pvgViewer.getRootNode();
	osg::ref_ptr<osg::Group> annoGroup = new osg::Group();
	osg::ref_ptr<osg::Group> modelGroup = new osg::Group();

	rootNode->addChild(annoGroup);
	rootNode->addChild(modelGroup);

	osg::ref_ptr<osgEarth::QtGui::DataManager> dataManager = new osgEarth::QtGui::DataManager(mapNode);
	DemoMainWindow appWin(dataManager.get(), mapNode, annoGroup);

	MyTableViewPopup* tvp = new MyTableViewPopup();
	ModelHandler modHandler = ModelHandler( mapNode, modelGroup, tvp );
	GeoTiffHandler geoTiffHandler = GeoTiffHandler( mapNode->getMap() );

	modHandler.loadModelsInFolder( std::string("data/modelosMontalegre"), true );
	geoTiffHandler.loadImageLayersInFolder(std::string("data/ortosMontalegre"));

	//geoTiffHandler.loadElevationLayersInFolder(std::string("data/dtm"));
	modHandler.loadModelsInFolder( std::string("data/pois"), true );

	pvgViewer.startRender();

	osgEarth::QtGui::ViewVector views;
	osgEarth::QtGui::ViewerWidget* viewerWidget = 0L;

	viewerWidget = new osgEarth::QtGui::ViewerWidget( pvgViewer.getViewer() );
	appWin.setViewerWidget(viewerWidget);

	viewerWidget->getViewer()->setThreadingModel(osgViewer::ViewerBase::SingleThreaded);

	appWin.setGeometry(100, 100, 1280, 800);
	appWin.show();

	return app.exec();
}
