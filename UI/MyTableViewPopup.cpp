#include "MyTableViewPopup.hpp"

#include <QHeaderView>

MyTableViewPopup::MyTableViewPopup()
{
	_model = NULL;
	_view = NULL;
	_onScreen = false;
	this->setWindowFlags(Qt::Popup);
}

MyTableViewPopup::MyTableViewPopup( std::vector<std::string> fields )
	: _fields(fields)
{
	this->setWindowFlags(Qt::Popup);

	setupTableView();
	_onScreen = false;
}

void MyTableViewPopup::setFields( std::vector< std::string > fields)
{
	_fields = fields;
	setupTableView();
}

void MyTableViewPopup::setupTableView()
{
	_model = createTableModel();
	_view = new QTableView( this );
	_view->setEditTriggers(QAbstractItemView::NoEditTriggers);
}

void MyTableViewPopup::fillFields( std::vector< std::string > vvalues)
{
	int i = 0;
	for( std::vector<std::string>::iterator it = vvalues.begin(); it != vvalues.end(); it++, i++ )
	{
		QStandardItem* aux = new QStandardItem( QString(it->c_str()) );
		_model->setItem(i, 0, aux);
	}
}

void MyTableViewPopup::drawPopup()
{
	prepareView();
	_onScreen = true;

	_view->show();
	this->show();
}

bool MyTableViewPopup::isOnScreen()
{
	return _onScreen;
}

void MyTableViewPopup::closeEvent( QCloseEvent *event )
 {
	_fields.clear();
	_model->clear();
	_view->close();

	_onScreen = false;
	event->accept();
 }

QStandardItemModel* MyTableViewPopup::createTableModel()
{
	QStandardItemModel* model = new QStandardItemModel();
	QStringList labels;

	int i = 0;
	for( std::vector<std::string>::iterator it = _fields.begin(); it != _fields.end(); it++, i++ )
	{
		labels.append( QString(it->c_str()) );
	}
	model->setVerticalHeaderLabels(labels);

	for(unsigned int i = 0; i < _fields.size(); i++ )
	{
		model->verticalHeaderItem(i)->setTextAlignment(Qt::AlignCenter);
	}

	return model;
}

void MyTableViewPopup::prepareView()
{
	_view->setModel(_model);
	_view->horizontalHeader()->hide();

	_view->resizeColumnsToContents();
	if(_view->columnWidth(0) > 400) _view->setColumnWidth(0,400);
	if(_view->columnWidth(0) < 200) _view->setColumnWidth(0,200);

	_view->resizeRowsToContents();
	int hei = getRowsHeight();

	QRect rect = _view->geometry();
	rect.setWidth( 2 + _view->verticalHeader()->width() + _view->columnWidth(0) );
	rect.setHeight( 2 + hei );
	_view->setGeometry(rect);

	QFont font;
	font.setBold(true);
	_view->verticalHeader()->setFont(font);
}


int MyTableViewPopup::getRowsHeight()
{
	int result = 0;

	for( unsigned int i = 0; i < _fields.size(); i++)
	{
		result += _view->rowHeight(i);
	}

	return result;
}

MyTableViewPopup::~MyTableViewPopup()
{

}
