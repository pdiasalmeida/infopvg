#ifndef MYTABLEVIEWPOPUPCPP_HPP_
#define MYTABLEVIEWPOPUPCPP_HPP_

#include <QWidget>
#include <QTableView>
#include <QStandardItemModel>
#include <QCloseEvent>

class MyTableViewPopup : public QWidget {
public:
	MyTableViewPopup();
	MyTableViewPopup( std::vector< std::string > fields );

	void setFields( std::vector< std::string > fields);
	void fillFields( std::vector< std::string > vvalues);

	void setupTableView();

	bool isOnScreen();
	void drawPopup();

	virtual ~MyTableViewPopup();

protected:
	std::vector<std::string> _fields;
	QTableView* _view;
	QStandardItemModel* _model;
	bool _onScreen;

	virtual void closeEvent( QCloseEvent *event );

private:
	QStandardItemModel* createTableModel();
	void prepareView();
	int getRowsHeight();
};

#endif /* MYTABLEVIEWPOPUPCPP_HPP_ */
