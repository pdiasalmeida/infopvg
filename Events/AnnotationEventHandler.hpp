#ifndef ANNOTATIONEVENTHANDLERS_HPP_
#define ANNOTATIONEVENTHANDLERS_HPP_

#include "../UI/MyTableViewPopup.hpp"

#include <osgEarthUtil/AnnotationEvents>

class MyAnnoEventHandler : public osgEarth::Util::AnnotationEventHandler {
public:
	MyAnnoEventHandler();
	MyAnnoEventHandler( MyTableViewPopup* tvp );

    void onHoverEnter( osgEarth::Annotation::AnnotationNode* anno, const EventArgs& args );
    void onHoverLeave( osgEarth::Annotation::AnnotationNode* anno, const EventArgs& args );

    virtual void onClick( osgEarth::Annotation::AnnotationNode* node, const EventArgs& details );

    virtual ~MyAnnoEventHandler();

protected:
    std::vector< std::string > _annoFields;
    MyTableViewPopup* _tvp;

private:
    void fillDefaultFields();
};

#endif /* ANNOTATIONEVENTHANDLERS_HPP_ */
