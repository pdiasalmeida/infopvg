#include "AnnotationEventHandler.hpp"

#include <osgEarthAnnotation/PlaceNode>

MyAnnoEventHandler::MyAnnoEventHandler()
{
	fillDefaultFields();
	_tvp = NULL;
}

MyAnnoEventHandler::MyAnnoEventHandler( MyTableViewPopup* tvp )
{
	fillDefaultFields();
	_tvp = tvp;
}

void MyAnnoEventHandler::onHoverEnter( osgEarth::Annotation::AnnotationNode* anno, const EventArgs& args )
{
	anno->setDecoration( "hover" );
}

void MyAnnoEventHandler::onHoverLeave( osgEarth::Annotation::AnnotationNode* anno, const EventArgs& args )
{
	anno->clearDecoration();
}

void MyAnnoEventHandler::onClick( osgEarth::Annotation::AnnotationNode* node, const EventArgs& details )
{
	( !(node->hasDecoration("click")) ) ? node->clearDecoration() : node->setDecoration("click");

	osgEarth::Annotation::PlaceNode* place = dynamic_cast<osgEarth::Annotation::PlaceNode*>(node);
	osgEarth::Annotation::AnnotationData* an = node->getAnnotationData();
	if( place == NULL )
	{
		OE_NOTICE << "Clicked Annotation" << std::endl;
		if( an != NULL )
		{
			double lat, lon = 0;
			an->getConfig().getIfSet("lat", lat);
			an->getConfig().getIfSet("lon", lon);

			std::cout << "Name: " << an->getName() << std::endl;
			std::cout << "Description: " << an->getDescription() << std::endl;
			std::cout << "Latitude: " << lat << std::endl;
			std::cout << "longitude: " << lon << std::endl;
		}
	}
	else if( !_tvp->isOnScreen() )
	{
		std::string id, tipo_nome, sub_tipo, concelho, localidade_postal, freguesia, descricao, fotos;
		std::vector< std::string > vvalues;

		vvalues.push_back(place->getText());
		place->getUserValue("poi_id_", id);
		vvalues.push_back(id);

		place->getUserValue("tipo_nome_", tipo_nome);
		place->getUserValue("sub_tipo_", sub_tipo);
		vvalues.push_back(tipo_nome);
		vvalues.push_back(sub_tipo);

		place->getUserValue("concelho_", concelho);
		place->getUserValue("localidade_postal_", localidade_postal);
		place->getUserValue("freguesia_", freguesia);
		vvalues.push_back(concelho);
		vvalues.push_back(localidade_postal);
		vvalues.push_back(freguesia);

		place->getUserValue("descricao_", descricao);
		place->getUserValue("fotos_", fotos);
		vvalues.push_back(descricao);
		vvalues.push_back(fotos);

		_tvp->setFields(_annoFields);
		_tvp->fillFields(vvalues);
		_tvp->drawPopup();
	}
}

void MyAnnoEventHandler::fillDefaultFields()
{
	_annoFields.push_back( std::string("Nome") );
	_annoFields.push_back( std::string("Id") );
	_annoFields.push_back( std::string("Tipo") );
	_annoFields.push_back( std::string("Subtipo") );
	_annoFields.push_back( std::string("Concelho") );
	_annoFields.push_back( std::string("Localidade postal") );
	_annoFields.push_back( std::string("Freguesia") );
	_annoFields.push_back( std::string("Descrição") );
	_annoFields.push_back( std::string("Fotos") );
}

MyAnnoEventHandler::~MyAnnoEventHandler()
{

}
