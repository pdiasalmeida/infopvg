#include "FileHelper.hpp"

#include <iostream>

#include <dirent.h>

#include <algorithm>

FileHelper::FileHelper()
{

}

void FileHelper::getFilesInDirectory( const std::string& dirName, std::vector< std::string >& fileNames,
		const std::vector< std::string >& validExtensions)
{
    // std::cout << "Opening directory " << dirName << "..." << std::endl;

    struct dirent* ep;
    size_t extensionLocation;

    DIR* dp = opendir( dirName.c_str() );
    if( dp != NULL )
    {
    	while( (ep = readdir(dp)) )
    	{
            if( ep->d_type & DT_DIR )
            {
            	std::string foundDirName = std::string(ep->d_name);
            	if( foundDirName.compare(".")!=0 && foundDirName.compare("..")!=0 )
            	{
            		getFilesInDirectory( dirName+"/"+foundDirName, fileNames, validExtensions );
            	}

            	continue;
            }

            extensionLocation = std::string( ep->d_name ).find_last_of( "." );
            std::string tempExt = toLowerCase( std::string( ep->d_name ).substr( extensionLocation + 1 ) );

            if( find(validExtensions.begin(), validExtensions.end(), tempExt) != validExtensions.end() )
            {
                // std::cout << "Found matching data file '" << ep->d_name << "'." << std::endl;
                fileNames.push_back( (std::string) dirName + "/" + ep->d_name );
            }
            else
            {
                // std::cout << "Found file does not match required file type, skipping: '" << ep->d_name << "'!" << std::endl;
            }
        }
        closedir( dp );
    }
    else
    {
        // std::cout << "Error opening directory " << dirName << "!" << std::endl;
    }
    return;
}

std::string FileHelper::toLowerCase( std::string in )
{
	std::transform( in.begin(), in.end(), in.begin(), ::tolower );

	return in;
}

FileHelper::~FileHelper()
{

}
