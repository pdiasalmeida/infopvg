#ifndef FILEHELPER_HPP_
#define FILEHELPER_HPP_

#include <string>
#include <vector>

class FileHelper {
public:
	FileHelper();

	void getFilesInDirectory( const std::string& dirName, std::vector< std::string >& fileNames,
			const std::vector< std::string >& validExtensions);

	~FileHelper();

protected:

private:
	std::string toLowerCase( std::string in );

};

#endif /* FILEHELPER_HPP_ */
