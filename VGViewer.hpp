#ifndef VGVIEWER_HPP_
#define VGVIEWER_HPP_

#include <osgViewer/Viewer>

#include <osgEarth/MapNode>

#include <osgEarthUtil/ExampleResources>

class VGViewer {
public:
	VGViewer();

	void setupVirtualGlobe( osgEarth::Viewpoint homeVP =
			osgEarth::Viewpoint("Home", osg::Vec3d(-7.791997579276f, 41.82319689018f, 0.0 ), 0.0, 0.0, 5000 ),
			bool attachSkyNode = true );
	void startRender();

	osgEarth::Util::MapNode* getMapNode();
	osg::ref_ptr<osg::Group> getRootNode();
	osg::ref_ptr<osgViewer::Viewer> getViewer();

	~VGViewer();

protected:
	osg::ref_ptr<osgViewer::Viewer> _viewer;
	osg::ref_ptr<osg::Group> _root;
	osg::ref_ptr<osgEarth::MapNode> _mapNode;
	osgEarth::Util::EarthManipulator* _manip;

private:
	void addSkyNode();

	void setBaseLayerTMS(std::string url);
	void removeLayer( std::string name );

};



#endif /* VGVIEWER_HPP_ */
