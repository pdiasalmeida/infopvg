# coding: utf-8

import csv
from lxml import etree
from pykml.factory import KML_ElementMaker as KML

def makeExtendedDataElements(datadict):
    '''Converts a dictionary to ExtendedData/Data elements'''
    edata = KML.ExtendedData()
    for key, value in datadict.iteritems():
        edata.append(KML.Data(KML.value(unicode(value, 'utf-8')), name=unicode(key, 'utf-8') + "_"))
    return edata

# create a KML document with a folder and a default style
stylename = "earthquake-balloon-style"
balloonstyle = KML.BalloonStyle(
    KML.text("""
<table Border=1>
  <tr><th>POI ID</th><td>$[poi_id_]</td></tr>
  <tr><th>Nome</th><td>$[nome_]</td></tr>
  <tr><th>Tipo</th><td>$[tipo_nome_]</td></tr>
  <tr><th>SubTipo</th><td>$[sub_tipo_]</td></tr>
  <tr><th>Coordenadas</th><td>($[xll_],$[yll_])</td></tr>
  <tr><th>Descricao</th><td>$[descricao_]</td></tr>
</table>"""
    ),
)

doc = KML.Document()

doc.append(
    KML.Style(
        KML.IconStyle(
                KML.color("ff000000"),
                KML.scale(1.0),
                KML.Icon(
                    KML.href("http://maps.google.com/mapfiles/kml/shapes/earthquake.png"),
                ),
                KML.hotSpot(x="0.5",y="0",xunits="fraction",yunits="fraction"),
            ),
        balloonstyle,
        id="poi-style"
    )
)

doc.append(KML.Folder())

# read in a csv file, and create a placemark for each record
with open('export.csv', 'rb') as csvfile:
    for row in csv.DictReader(csvfile):
        pm = KML.Placemark(
            KML.name(unicode(row['nome'], 'utf-8')),
            KML.styleUrl("#poi-style"),
            makeExtendedDataElements(row),
            KML.Point(
                KML.coordinates("{0},{1}".format(row["xll"],row["yll"]))
            )
        )
        doc.Folder.append(pm)

# check if the schema is valid
from pykml.parser import Schema
schema_gx = Schema("kml22gx.xsd")
schema_gx.assertValid(doc)

f = open('pois.kml','w')
f.write(etree.tostring(doc, encoding=unicode,pretty_print=True)) # python will convert \n to os.linesep
f.close()
