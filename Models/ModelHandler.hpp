#ifndef MODELHANDLER_HPP_
#define MODELHANDLER_HPP_

#include <osgEarthUtil/ExampleResources>

#include "../Auxiliar/FileHelper.hpp"
#include "../UI/MyTableViewPopup.hpp"

class ModelHandler {
public:
	ModelHandler( osgEarth::Util::MapNode* _mapNode, osg::ref_ptr<osg::Group> root, MyTableViewPopup* tvp );

	void loadModelsInFolder( std::string dirName, bool inches );
	void addModel( std::string address, bool inches );

	~ModelHandler();

protected:
	osgEarth::Util::MapNode* _mapNode;
	osg::ref_ptr<osg::Group> _root;
	FileHelper* _fHelper;
	MyTableViewPopup* _tvp;

private:
	ModelHandler();

};

#endif /* MODELHANDLER_HPP_ */
