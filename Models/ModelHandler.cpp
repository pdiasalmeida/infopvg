#include "ModelHandler.hpp"
#include "../Events/AnnotationEventHandler.hpp"

#include <osgEarthDrivers/kml/KML>

#include <osgEarthAnnotation/AnnotationData>
#include <osgEarthAnnotation/HighlightDecoration>
#include <osgEarthAnnotation/ScaleDecoration>

#include <osgEarthUtil/AnnotationEvents>
#include <osgEarthUtil/ObjectLocator>

ModelHandler::ModelHandler()
{
	_mapNode = NULL;
	_fHelper = NULL;
	_tvp = NULL;
}

ModelHandler::ModelHandler(osgEarth::Util::MapNode* mapNode, osg::ref_ptr<osg::Group> root, MyTableViewPopup* tvp )
{
	_mapNode = mapNode;
	_root = root;
	_fHelper = new FileHelper();
	_tvp = tvp;
}

void ModelHandler::loadModelsInFolder( std::string dirName, bool inches )
{
	std::vector< std::string > filenames;
	std::vector< std::string > validExtensions;

	validExtensions.push_back("kml");
	_fHelper->getFilesInDirectory( dirName, filenames, validExtensions );

	for( std::vector< std::string >::iterator it = filenames.begin(); it != filenames.end(); it++ )
	{
		std::cout << *it << std::endl;
		addModel( *it, inches );
	}
}

void ModelHandler::addModel( std::string address, bool inches )
{
	if( _mapNode && _root )
	{
		osgDB::Options* opt = new osgDB::Options;


		osgEarth::Drivers::KMLOptions kml_options = osgEarth::Drivers::KMLOptions();
		if( inches ) kml_options.modelScale() = 0.0254;
		kml_options.declutter() = true;

		osg::ref_ptr<osg::Node> nodekml = osgEarth::Drivers::KML::load( osgEarth::URI( address ), _mapNode, opt, kml_options );

		osgEarth::Annotation::DecorationInstaller scaleInstaller("hover", new osgEarth::Annotation::ScaleDecoration(1.1f));
		nodekml->accept( scaleInstaller );

		osgEarth::Util::AnnotationEventCallback* cb = new osgEarth::Util::AnnotationEventCallback();
		cb->addHandler( new MyAnnoEventHandler(_tvp) );
		nodekml->addEventCallback( cb );

		_root->addChild(nodekml);
	}
}

ModelHandler::~ModelHandler()
{

}
