CC=			g++

.PHONY: all clean
.SECONDARY: main-build

CFLAGS=		-c -g -O0 -Wall -fPIC \
			-I/usr/include/qt5/QtWidgets -I/usr/include/qt5/QtGui -I/usr/include/qt5/QtCore -I/usr/include/qt5 -I/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++-64 -I/usr/include/qt5/QtOpenGL
LDFLAGS=	-losg -losgUtil -losgDB -losgGA -losgViewer -losgQt \
			-losgEarth -losgEarthUtil -losgEarthSymbology -losgEarthAnnotation -losgEarthQt \
			-lQt5Core -lQt5Gui -lQt5Widgets -lQt5OpenGL

SOURCES=	Auxiliar/FileHelper.cpp \
			Events/AnnotationEventHandler.cpp \
			Models/ModelHandler.cpp \
			Layers/GeoTiffHandler.cpp \
			UI/MyTableViewPopup.cpp \
			UI/qrc_images.cpp UI/moc_MainWindow.cpp \
			VGViewer.cpp main.cpp

OBJECTS=$(SOURCES:.cpp=.o)

UI_BASEDIR = UI/

RESOURCES=	$(UI_BASEDIR)images.qrc
RESOURCES_FILES=	$(RESOURCES:$(UI_BASEDIR)%.qrc=$(UI_BASEDIR)qrc_%.cpp)

MOC_SOURCE=	$(UI_BASEDIR)MainWindow.hpp
MOC_FILES= 	$(MOC_SOURCE:$(UI_BASEDIR)%.hpp=$(UI_BASEDIR)moc_%.cpp)

EXECUTABLE=infoPVG

all: pre-build main-build

pre-build: rcc moc

rcc: $(RESOURCES_FILES)
	@echo "Generated files: " $(RESOURCES_FILES)

qrc_%.cpp : %.qrc
	@echo "Generating " $@ " from " $< 
	rcc -o $@ $<

moc: $(MOC_FILES)
	@echo "Generated files: " $(MOC_FILES)

moc_%.cpp : %.hpp
	@echo "Generating " $@ " from " $< 
	moc -o $@ $<

main-build: $(SOURCES) $(EXECUTABLE) 
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@
	
clean:
	rm -f $(RESOURCES_FILES) $(MOC_FILES) $(OBJECTS) $(EXECUTABLE)