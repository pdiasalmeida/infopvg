#ifndef GEOTIFFHANDLER_HPP_
#define GEOTIFFHANDLER_HPP_

#include <osgEarth/Map>

#include "../Auxiliar/FileHelper.hpp"

class GeoTiffHandler {
public:
	GeoTiffHandler( osgEarth::Map* _map );

	void loadImageLayersInFolder(std::string dirName);
	void loadElevationLayersInFolder(std::string dirName);
	void addImageLayer(std::string address);
	void addElevationLayer(std::string address);

	~GeoTiffHandler();

protected:
	osgEarth::Map* _map;
	FileHelper* _fHelper;

private:
	GeoTiffHandler();

};

#endif /* GEOTIFFHANDLER_HPP_ */
