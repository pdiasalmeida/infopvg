#include "GeoTiffHandler.hpp"

#include <osgEarthDrivers/gdal/GDALOptions>

GeoTiffHandler::GeoTiffHandler()
{
	_map = NULL;
	_fHelper = NULL;
}

GeoTiffHandler::GeoTiffHandler(osgEarth::Map* map)
{
	_map = map;
	_fHelper = new FileHelper();
}

void GeoTiffHandler::loadImageLayersInFolder(std::string dirName)
{
	std::vector< std::string > filenames;
	std::vector< std::string > validExtensions;

	validExtensions.push_back("tiff");
	//validExtensions.push_back("tif");
	_fHelper->getFilesInDirectory( dirName, filenames, validExtensions );

	for( std::vector< std::string >::iterator it = filenames.begin(); it != filenames.end(); it++ )
	{
		std::cout << *it << std::endl;
		addImageLayer( *it );
	}
}

void GeoTiffHandler::loadElevationLayersInFolder(std::string dirName)
{
	std::vector< std::string > filenames;
	std::vector< std::string > validExtensions;

	validExtensions.push_back("tiff");
	validExtensions.push_back("tif");
	_fHelper->getFilesInDirectory( dirName, filenames, validExtensions );

	for( std::vector< std::string >::iterator it = filenames.begin(); it != filenames.end(); it++ )
	{
		std::cout << *it << std::endl;
		addElevationLayer( *it );
	}
}

void GeoTiffHandler::addImageLayer(std::string address)
{
	if( _map )
	{
		{
		    osgEarth::Drivers::GDALOptions gdal;
		    gdal.url() = address;

		    osgEarth::ImageLayer* layer = new osgEarth::ImageLayer( address, gdal );
		    _map->addImageLayer( layer );
		}
	}
}

void GeoTiffHandler::addElevationLayer(std::string address)
{
	if( _map )
	{
		{
		    osgEarth::Drivers::GDALOptions gdal;
		    gdal.url() = address;

		    osgEarth::ElevationLayer* layer = new osgEarth::ElevationLayer( address, gdal );
		    _map->addElevationLayer( layer );
		}
	}
}

GeoTiffHandler::~GeoTiffHandler()
{

}
