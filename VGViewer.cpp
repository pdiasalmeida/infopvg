#include "VGViewer.hpp"

#include <osgGA/GUIEventHandler>
#include <osgGA/StateSetManipulator>
#include <osgViewer/ViewerEventHandlers>

#include <osgEarth/Map>
#include <osgEarthUtil/SkyNode>

#include <osgEarthDrivers/tms/TMSOptions>
#include <osgEarthDrivers/cache_filesystem/FileSystemCache>


VGViewer::VGViewer()
{
	osg::DisplaySettings::instance()->setMinimumNumStencilBits( 2 );

	//create the viewer
	_viewer = new osgViewer::Viewer();
	_viewer->addEventHandler(new osgViewer::StatsHandler());
	_viewer->addEventHandler(new osgViewer::LODScaleHandler());

	_manip = NULL;
}

void VGViewer::setupVirtualGlobe( osgEarth::Viewpoint homeVP, bool attachSkyNode )
{
	osgEarth::Config conf;
	conf.add("reproject_before_caching",true);
	conf.add("path","data/cache");
	osgEarth::Drivers::FileSystemCacheOptions cacheOptions =
			osgEarth::Drivers::FileSystemCacheOptions(osgEarth::ConfigOptions(conf));

	osgEarth::MapOptions mapOptions;
	mapOptions.cache() = cacheOptions;

	osgEarth::Map* map = new osgEarth::Map(mapOptions);
	_mapNode = new osgEarth::MapNode( map );

	setBaseLayerTMS("http://readymap.org/readymap/tiles/1.0.0/35/");
	osgEarth::Drivers::TMSOptions tms;
	tms.url() = "http://readymap.org/readymap/tiles/1.0.0/9/";

	osgEarth::ElevationLayer* layer = new osgEarth::ElevationLayer( "BASEE", tms );
	_mapNode->getMap()->addElevationLayer( layer );

	_root = new osg::Group();

	_root->addChild(_mapNode);
	if( attachSkyNode )
	{
		addSkyNode();
	}

	_viewer->setCameraManipulator( new osgEarth::Util::EarthManipulator() );
	_manip = dynamic_cast<osgEarth::Util::EarthManipulator*>(_viewer->getCameraManipulator());
	_manip->setHomeViewpoint(homeVP);

	_viewer->addEventHandler(new osgGA::StateSetManipulator(_viewer->getCamera()->getOrCreateStateSet()));
}

void VGViewer::startRender()
{
	if(_root)
	{
		_viewer->setSceneData(_root);
		// configure the near/far so we don't clip things that are up close
		_viewer->getCamera()->setNearFarRatio(0.00002);
		// _viewer->run();
	}
	else
	{
		OE_NOTICE
			<< "\nUsage: Please setup the viewer first" << std::endl;
	}
}

osgEarth::Util::MapNode* VGViewer::getMapNode()
{
	return _mapNode.get();
}
osg::ref_ptr<osg::Group> VGViewer::getRootNode()
{
	return _root;
}

osg::ref_ptr<osgViewer::Viewer> VGViewer::getViewer()
{
	return _viewer;
}

void VGViewer::addSkyNode()
{
	osg::Light* light = new osg::Light( 0 );
	light->setPosition( osg::Vec4(0, -1, 0, 0 ) );
	light->setAmbient( osg::Vec4(0.4f, 0.4f, 0.4f ,1.0) );
	light->setDiffuse( osg::Vec4(1,1,1,1) );
	light->setSpecular( osg::Vec4(0,0,0,1) );
	_root->getOrCreateStateSet()->setAttribute(light);

	double hours = 12.0f;
	float ambientBrightness = 0.4f;
	osgEarth::Util::SkyNode* sky = new osgEarth::Util::SkyNode( _mapNode->getMap() );
	sky->setAmbientBrightness( ambientBrightness );
	sky->setDateTime( osgEarth::DateTime(1984, 11, 8, hours) );
	sky->attach( _viewer, 0 );

	_root->addChild( sky );
}

void VGViewer::setBaseLayerTMS(std::string url)
{
	osgEarth::MapNode* mapNode = _mapNode.get();
	if(mapNode)
	{
		osgEarth::ImageLayerVector layers;
		mapNode->getMap()->getImageLayers(layers);

		for( osgEarth::ImageLayerVector::const_iterator it = layers.begin(); it != layers.end(); ++it )
		{
			if( ((*it)->getTerrainLayerRuntimeOptions().name()).compare("BASE") == 0 )
			{
				mapNode->getMap()->removeImageLayer( (*it).get() );
			}
		}

		osgEarth::Config conf;
		conf.add("url",url);
		conf.add("cache_policy","no_cache");

		osgEarth::Drivers::TMSOptions tms = osgEarth::Drivers::TMSOptions(osgEarth::ConfigOptions(conf));
		osgEarth::ImageLayer* layer = new osgEarth::ImageLayer( "BASE", tms );
		mapNode->getMap()->addImageLayer( layer );

		OE_NOTICE << "VGViewer::setBaseLayerTMS	" << url << std::endl;
	}
}

void VGViewer::removeLayer( std::string name )
{
	osgEarth::Util::MapNode* mapNode = _mapNode.get();
	if(mapNode)
	{
		osgEarth::ImageLayerVector layers;
		mapNode->getMap()->getImageLayers(layers);

		for( osgEarth::ImageLayerVector::const_iterator it = layers.begin(); it != layers.end(); ++it )
		{
			if( ((*it)->getTerrainLayerRuntimeOptions().name()).compare(name) == 0 )
			{
				mapNode->getMap()->removeImageLayer( (*it).get() );
			}
		}
	}

	OE_NOTICE <<" VGViewer::removeLayer " << name << std::endl;
}

VGViewer::~VGViewer()
{

}
